/*
 * algo2.c
 *
 *  Created on: Sep 16, 2019
 *      Author: AM39720
 */


#include "algo2.h"


int* algo2( int* T, int* G, int a, int* D, int b )
{
	int i = 0, j = 0, k = 0;

	while ( ( i < a ) & ( j < b ) )
	{

		if ( G[ i ] < D[ j ] )
		{
			compteur_algo ++;
			T[ k ] = G[ i ];
			i++;
		}
		else
		{
			compteur_algo ++;
			T[ k ] = D[ j ];
			j++;
		}
		k++;
	}

	while( i < a )
	{
		compteur_algo ++;

		T[ k ] = G[ i ];
		i++;
		k++;
	}

	while( j < b )
	{
		compteur_algo ++;

		T[ k ] = D[ j ];
		j++;
		k++;
	}

	return T;

}

