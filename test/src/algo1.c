
#include <algo1.h>
#include "algo2.h"

int* algo1(int* T, int N){

	if (N>=2){

		int m = N / 2;
		int G[m];
		int D[N-m];

		for(int i=0;i <= m-1;i++){

			compteur_algo ++;

			G[i]=T[i];
		}

		for(int j=0;j <= N-m-1;j++){

			compteur_algo ++;

			D[j]=T[m+j];
		}

		algo1(G, m);
		algo1(D, N-m);
		T = algo2(T,G,m,D,N-m);

	}

	return T;

}

