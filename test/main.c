#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "algo1.h"
#include "algo2.h"
#include "ressources.h"

/******************************************************************************/
/*                  DEFINITIONS DE TYPES ET DE CONSTANTES                     */
/******************************************************************************/
#define INITIAL_VALUE 4
#define SIZE_BAROMETRE 10

int compteur_algo = INITIAL_VALUE;

/******************************************************************************/
/*                  DÉCLARATION DU PROGRAMME PRINCIPAL                        */
/******************************************************************************/

int main(void)
{
	clock_t start,end;
	double cpu_time_used;

	int R[] = {100,10000,1000000,100000000};
	int D[] = {0,25,50,75,100};
	int N = 0;
	int* T;
	int* G;
	int barometre[10];

	char* nom_fichier = "Labo1-Test";


	for( int i = 1; i <= 10; i++ )
	{
		N = i * 1000;

		for( int j = 0; j <= 3; j++)
		{
			for ( int k = 0; k <= 4; k++)
			{
				for( int x = 0; x <= 9; x++)
				{

					T = generer_donnees(N,R[ j ],D[ k ]);
					G = algo1(T,N);
					barometre[x] = compteur_algo;
					compteur_algo = INITIAL_VALUE;

				}

				sauvegarder(N, R[j], D[k],
						barometre, SIZE_BAROMETRE, nom_fichier);

			}
		}
	}

    return EXIT_SUCCESS;
}
